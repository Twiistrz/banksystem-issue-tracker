BankSystem is now a premium plugin. Will be available on MC-Market soon.

BankSystem allows you to safely store your money in other to avoid overspending. For example, this can be used for your market shops so you don't end up losing all your money if your shop has some very favorable trades.

Official Plugin Compatibility: -

Dependencies:

 * Vault
 * Economy System that works with Vault
 * Permission System the works with Vault

Soft Dependency:

 * PlaceholderAPI

Placeholders:

 * -
	
Features:

 * MySQL DataSource.
 * The plugin saves the player's information by UUID of the player.
 * Supports any Economy system that works with Vault.
 * Sound effects when using the bank system.
 * Money format, Messages, Settings, and many customizations
 * Interest rate when the player is online.
 * Override default balance command in-game.

Join our Discord Support Server
https://discord.gg/E4T59v99PU